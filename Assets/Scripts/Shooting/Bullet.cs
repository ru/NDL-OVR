using UnityEngine;
using Leap;

// Voeg dit script toe aan een BulletController (Empty Object) en voeg dan een Projectiel toe aan dit script als Bullet
public class Bullet : MonoBehaviour
{
	
	private Leap.Controller controller;
	
	// projectile
	public GameObject bullet;
	public GameObject ethan;
	public GameObject head;
	public GameObject camera;

	public Vector3 dir; 

	private float upward = 1.5f;
	public float forward;

	private float time = 0;

	public void Start ()
	{
		if (bullet == null) {
			return;
		}
		controller = new Controller ();
	}

	void Update ()
	{
		time += Time.deltaTime;
		if (time < .4)
			return;

		if (controller == null)
			return;
    
		Frame frame = controller.Frame ();
		foreach (Hand hand in frame.Hands) {
			time = 0;
			GameObject sphere = Instantiate (bullet, transform.position, transform.rotation) as GameObject;
			Destroy (sphere, 2f);
			
			
			Vector3 bulletMovement = camera.transform.rotation * new Vector3 (0,0,1000);
			
			sphere.GetComponent<Rigidbody>().AddForce (bulletMovement);
			sphere.GetComponent<Rigidbody>().MovePosition (ethan.transform.position + new Vector3(0,upward,0));
		}
	}

	public static Vector3 leapVectorToUnity (Vector vector)
	{
		return new Vector3 (vector.x, vector.y, vector.z) * 1000;
	}
}


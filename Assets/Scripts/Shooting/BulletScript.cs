﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour
{
	private static string[] nonColliderTags = {
		"Player",
		"Hand",
		"Enemy",
		"EnemyBody",
		"NonBulletCollider",
		"Door"
	};
	
	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.CompareTag ("EnemyBody")) {
			other.GetComponentInParent<EnemyAnimation> ().die ();
		}

		foreach (string s in nonColliderTags) {
			if (other.gameObject.CompareTag (s))
				return;
		}

		Destroy (this.gameObject);
	}
	
}
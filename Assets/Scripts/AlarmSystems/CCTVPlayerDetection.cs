﻿using UnityEngine;
using System.Collections;

public class CCTVPlayerDetection : MonoBehaviour {

	private GameObject player;
	private LastPlayerSighted lastPlayerSighted;

	void Awake () {
		player = GameObject.FindGameObjectWithTag (Tags.player);
		lastPlayerSighted = GameObject.FindGameObjectWithTag (Tags.gameController).GetComponent<LastPlayerSighted> ();
	}

	void OnTriggerStay (Collider other) {
		if (other.gameObject == player) {
			Vector3 relPlayerPos = player.transform.position - transform.position;
			RaycastHit hit;

			if (Physics.Raycast (transform.position, relPlayerPos, out hit)) {
				if (hit.collider.gameObject == player) {
					lastPlayerSighted.position = player.transform.position;
				}
			}
		}
	}
}

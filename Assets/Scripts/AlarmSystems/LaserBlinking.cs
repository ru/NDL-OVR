﻿using UnityEngine;
using System.Collections;

public class LaserBlinking : MonoBehaviour {

	public float onTime;
	public float offTime;

	public float timer;

	void Update () {
		timer += Time.deltaTime;

		if (GetComponent<Renderer>().enabled && timer >= onTime ||
		    !GetComponent<Renderer>().enabled && timer >= offTime) {
			SwitchBeam ();
		}
	}

	void SwitchBeam () {
		timer = 0f;
		GetComponent<Renderer>().enabled = !GetComponent<Renderer>().enabled;
		GetComponent<Light>().enabled = !GetComponent<Light>().enabled;
	}
}

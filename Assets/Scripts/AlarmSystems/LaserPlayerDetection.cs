﻿using UnityEngine;
using System.Collections;

public class LaserPlayerDetection : MonoBehaviour {

	private GameObject player;
	private LastPlayerSighted lastPlayerSighted;

	void Awake () {
		player = GameObject.FindGameObjectWithTag (Tags.player);
		lastPlayerSighted = GameObject.FindGameObjectWithTag (Tags.gameController).GetComponent<LastPlayerSighted> ();
	}

	void OnTriggerStay (Collider other) {
		if (GetComponent<Renderer>().enabled) {
			if (other.gameObject == player) {
				lastPlayerSighted.position = other.transform.position;
			}
		}
	}
}

using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	public AudioClip shoutingClip;
	public float turnDamp = 15f;
	public float correctionDamp = 1f;
	public float forwardDamp = 0.1f;
	public float sidewardDamp = 1f;
	public float turnSpeed = 200;
	public float forwardSpeed = 5.5f;
	public float backwardSpeed = -5.5f;

	public Vector3 cameraCorrection = new Vector3 (0, 8, 0);

	private Animator anim;
	private HashIDs hash;
	private OVRCameraRig ovr;

	private float speed;
	private float rotationSpeed;

	private Quaternion rotationQuat;
	public Quaternion walkingRotation;
	public Quaternion walkingHeadRotation;
	
	private Quaternion cameraCorrectionStart;
	private Quaternion OVRDelta;

	void Awake () {
		anim = GetComponent<Animator> ();
		hash = GameObject.FindGameObjectWithTag (Tags.gameController).GetComponent<HashIDs> ();
		ovr = GameObject.FindGameObjectWithTag (Tags.ovr).GetComponent<OVRCameraRig> ();
		anim.SetLayerWeight (1, 1f);

		rotationSpeed = 0;
		rotationQuat = Quaternion.identity;
		OVRDelta = Quaternion.identity;

		rotationQuat = GetComponent<Rigidbody>().rotation;
		walkingRotation = Quaternion.Euler (0, 0, 0);

		cameraCorrectionStart = transform.Find ("char_ethan_skeleton/char_ethan_Hips/char_ethan_Spine/char_ethan_Spine1/char_ethan_Spine2/char_ethan_Neck/char_ethan_Head/char_ethan_Head1/OVRCameraRig").GetComponent<Transform> ().localRotation;
	}

	void LateUpdate () {
		float h = Input.GetAxis ("Horizontal");
		float v = Input.GetAxis ("Vertical");
		bool sneak = Input.GetButton ("Sneak");
		float turning = Input.GetAxis ("TurnRight");

		SetMovementCorrection ();
		MovementManagement (h, v, sneak, turning);
	}

	void Update () {
		bool shout = Input.GetButtonDown ("Attract");
		anim.SetBool (hash.shoutingBool, shout);
		AudioManagement (shout);
	}

	void MovementManagement (float horizontal, float vertical, bool sneaking, float turning) {
		// Prevent moving if disabled
		if (!enabled)
			return;

		// Set sneaking
		anim.SetBool (hash.sneakingBool, sneaking);

		// Compute rotation to walk sidewards
		walkingRotation = Quaternion.Euler (0, computeWalkingCorrection (horizontal, vertical), 0);
		walkingRotation = Quaternion.Lerp (walkingRotation, walkingRotation, sidewardDamp * Time.deltaTime);
		GetComponent<Rigidbody> ().MoveRotation (rotationQuat * OVRDelta * walkingRotation);

		// Compute rotating with Q and E
		rotationSpeed = Mathf.Lerp (rotationSpeed, turning, turnDamp * Time.deltaTime);
		Vector3 turnSpeedVector = new Vector3 (0, turnSpeed, 0);
		Quaternion deltaRotation = Quaternion.Euler (turnSpeedVector * Time.deltaTime * rotationSpeed);
		rotationQuat = rotationQuat * deltaRotation;

		// Get rotation from looking with OVR
		Vector3 vector = ovr.OVRTransform.eulerAngles;
		vector.x = 0;
		vector.z = 0;
		OVRDelta = Quaternion.Euler (vector);

		// Compute and set forward speed
		speed = computeSpeed (horizontal, vertical);
		anim.SetFloat (hash.forwardFloat, speed, forwardDamp, Time.deltaTime);
	}

	void SetMovementCorrection () {
		// Set head correction
		walkingHeadRotation = Quaternion.Euler (walkingRotation.eulerAngles.y, 0, 0);
		transform.Find ("char_ethan_skeleton/char_ethan_Hips/char_ethan_Spine/char_ethan_Spine1/char_ethan_Spine2/char_ethan_Neck/char_ethan_Head").GetComponent<Transform> ().rotation *= walkingHeadRotation;
		
		setOVRCorrection (walkingRotation.eulerAngles.y, speed);
	}

	/* 
	 * 293
	 * 306
	 * 56
	 * 
	 * 29
	 * 78
	 * 264
	 */

	float computeWalkingCorrection (float hor, float ver) {
		// Avoid devision by 0
		if (ver == 0)
			ver = 0.000000000000000000000001f;
		// Return the angle
		return Mathf.Rad2Deg * Mathf.Atan (hor / ver);
	}

	float computeSpeed (float hor, float ver) {
		// Compute total movement vector
		float movement = Mathf.Sqrt (hor * hor + ver * ver);
		// Cap movement to 1
		if (movement > 1)
			movement = 1;
		// Return backward speed
		if (ver < 0) {
			return backwardSpeed * movement;
		}
		// Return forward speed
		return forwardSpeed * movement;
	}

	void setOVRCorrection (float angle, float speed) {

		// Transpose angle to correct domain
		if (angle <= 91)
			angle = angle;
		else if (angle < 269)
			angle -= 180;
		else 
			angle -= 360;

		Quaternion correction = Quaternion.Lerp (
				transform.Find ("char_ethan_skeleton/char_ethan_Hips/char_ethan_Spine/char_ethan_Spine1/char_ethan_Spine2/char_ethan_Neck/char_ethan_Head/char_ethan_Head1/OVRCameraRig")
				.GetComponent<Transform> ()
				.localRotation, 
			Quaternion.Euler (
				cameraCorrection * safeSqrt((angle / 90) * Mathf.Abs(speed)))
			* cameraCorrectionStart, correctionDamp * Time.deltaTime);
		transform.Find ("char_ethan_skeleton/char_ethan_Hips/char_ethan_Spine/char_ethan_Spine1/char_ethan_Spine2/char_ethan_Neck/char_ethan_Head/char_ethan_Head1/OVRCameraRig")
			.GetComponent<Transform> ()
			.localRotation = correction;
	}

	void AudioManagement (bool shout) {
		if (anim.GetCurrentAnimatorStateInfo (0).nameHash == hash.locomotionState) {
			if (!GetComponent<AudioSource>().isPlaying) {
				GetComponent<AudioSource>().Play ();
			}
		} else {
			GetComponent<AudioSource>().Stop ();
		}

		if (shout) {
			AudioSource.PlayClipAtPoint (shoutingClip, transform.position);
		}
	}

	private float safeSqrt (float x) {
		if (x < 0)
			return -Mathf.Sqrt (-x);
		return Mathf.Sqrt (x);
	}
}
